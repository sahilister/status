---
title: "Gitlab 13.4.6 feature update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2020-12-03 16:40:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2020-12-03 18:15:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
We updated gitlab to 13.4.6 feature release. We ran out of memory during the
update, so it took more time than usual.
